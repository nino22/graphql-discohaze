require('dotenv').config();
const { env } = process;

module.exports = {
  apiUrl: env.API_URL,
  apiKey: env.API_KEY,
  brazeEndpoint: env.BRAZE_ENDPOINT,
  brazeApiKey: env.BRAZE_API_KEY,
  apiPredictionUrl: env.API_URL_PREDICTION,
  spreeClientToken: env.SPREE_CLIENT_TOKEN,
  clientType: env.CLIENT_TYPE,
  adyenKey: env.ADYEN_KEY,
  sentryDSN: env.SENTRY_DSN,
  corporateFormID: env.CORPORATE_FORM_ID,
  corporateFormMY: env.CORPORATE_FORM_MY,
  corporateFormTH: env.CORPORATE_FORM_TH,
  FBAppId: env.FACEBOOK_APP_ID,
  apptimizeAppKey: env.APPTIMIZE_APP_KEY,
};
