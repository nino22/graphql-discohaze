const { gql } = require("apollo-server");

const schema = gql`
  type Product {
    id: Int
    name: String
    description: String
    available_on: String
    shipping_category_id: Int
    price: string
  }
  
  type Query {
    product(storeId: Int, id: Int): Product
  }
`;

module.exports = schema;