const { ApolloServer } = require("apollo-server");
const schema = require('./schema/index');
const ProductAPI = require("./source/product");


// Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves books from the "books" array above.
const resolvers = {
  Query: {
    product: async (_, { storeId, id }, { dataSources }) => {
      this.storeId = storeId;
      return dataSources.productAPI.getProduct(storeId, id);
    }
  }
};

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  dataSources: () => {
    return {
      productAPI: new ProductAPI()
    };
  },
});

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
