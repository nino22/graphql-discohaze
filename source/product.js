const DiscohazeDataSource = require("./index");

class Product extends DiscohazeDataSource {
  // GET Product
  async getProduct(storeId, id) {
    return this.get(`sprinkles/stock_locations/${storeId}/products/${id}`);
  }
}

module.exports = Product;
