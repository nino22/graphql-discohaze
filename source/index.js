const { RESTDataSource } = require('apollo-datasource-rest');
const { apiUrl, clientType, apiKey, spreeClientToken } = require('../config/env.config');

class Discohaze extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = apiUrl;
  }

  // Intercept
  willSendRequest(request) {
    request.headers.set('X-Happy-Client-Type', clientType);
    request.headers.set('X-API-Key', apiKey);
    request.headers.set('X-Spree-Client-Token', spreeClientToken);
    request.headers.set('Country', 'ID');
    request.headers.set('Locale', 'id');
  }
}

module.exports = Discohaze;
